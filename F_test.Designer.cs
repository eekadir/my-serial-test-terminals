﻿namespace test
{
    partial class F_test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_Receive = new System.Windows.Forms.RichTextBox();
            this.btnSerialCom = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cb_COMs = new System.Windows.Forms.ComboBox();
            this.rtb_Send = new System.Windows.Forms.RichTextBox();
            this.cb_Baud = new System.Windows.Forms.ComboBox();
            this.button_up = new System.Windows.Forms.Button();
            this.button_dw = new System.Windows.Forms.Button();
            this.button_lft = new System.Windows.Forms.Button();
            this.button_right = new System.Windows.Forms.Button();
            this.button_Ok = new System.Windows.Forms.Button();
            this.button_esc = new System.Windows.Forms.Button();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.label_fl = new System.Windows.Forms.Label();
            this.button_kapat = new System.Windows.Forms.Button();
            this.button_arrow_up = new System.Windows.Forms.Button();
            this.button_arrow_down = new System.Windows.Forms.Button();
            this.button_OverLoad = new System.Windows.Forms.Button();
            this.button_outofservice = new System.Windows.Forms.Button();
            this.button_gong = new System.Windows.Forms.Button();
            this.button_door_close = new System.Windows.Forms.Button();
            this.button_door_open = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtb_Receive
            // 
            this.rtb_Receive.Location = new System.Drawing.Point(163, 8);
            this.rtb_Receive.Margin = new System.Windows.Forms.Padding(1);
            this.rtb_Receive.Name = "rtb_Receive";
            this.rtb_Receive.Size = new System.Drawing.Size(373, 202);
            this.rtb_Receive.TabIndex = 0;
            this.rtb_Receive.Text = "";
            // 
            // btnSerialCom
            // 
            this.btnSerialCom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerialCom.Location = new System.Drawing.Point(9, 69);
            this.btnSerialCom.Name = "btnSerialCom";
            this.btnSerialCom.Size = new System.Drawing.Size(125, 34);
            this.btnSerialCom.TabIndex = 1;
            this.btnSerialCom.Text = "Connect";
            this.btnSerialCom.UseVisualStyleBackColor = true;
            this.btnSerialCom.Click += new System.EventHandler(this.btnSerialCom_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(12, 406);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 37);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cb_COMs
            // 
            this.cb_COMs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_COMs.FormattingEnabled = true;
            this.cb_COMs.Location = new System.Drawing.Point(10, 11);
            this.cb_COMs.Name = "cb_COMs";
            this.cb_COMs.Size = new System.Drawing.Size(126, 24);
            this.cb_COMs.TabIndex = 3;
            this.cb_COMs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cb_COMs_MouseDown);
            // 
            // rtb_Send
            // 
            this.rtb_Send.Location = new System.Drawing.Point(163, 211);
            this.rtb_Send.Margin = new System.Windows.Forms.Padding(1);
            this.rtb_Send.Name = "rtb_Send";
            this.rtb_Send.Size = new System.Drawing.Size(373, 215);
            this.rtb_Send.TabIndex = 4;
            this.rtb_Send.Text = "";
            // 
            // cb_Baud
            // 
            this.cb_Baud.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_Baud.FormattingEnabled = true;
            this.cb_Baud.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.cb_Baud.Location = new System.Drawing.Point(10, 40);
            this.cb_Baud.Name = "cb_Baud";
            this.cb_Baud.Size = new System.Drawing.Size(126, 24);
            this.cb_Baud.TabIndex = 5;
            // 
            // button_up
            // 
            this.button_up.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_up.Location = new System.Drawing.Point(43, 1);
            this.button_up.Margin = new System.Windows.Forms.Padding(1);
            this.button_up.Name = "button_up";
            this.button_up.Size = new System.Drawing.Size(40, 41);
            this.button_up.TabIndex = 6;
            this.button_up.Text = "Up";
            this.button_up.UseVisualStyleBackColor = true;
            this.button_up.Click += new System.EventHandler(this.button_up_Click);
            // 
            // button_dw
            // 
            this.button_dw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_dw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_dw.Location = new System.Drawing.Point(43, 87);
            this.button_dw.Margin = new System.Windows.Forms.Padding(1);
            this.button_dw.Name = "button_dw";
            this.button_dw.Size = new System.Drawing.Size(40, 42);
            this.button_dw.TabIndex = 7;
            this.button_dw.Text = "Dw";
            this.button_dw.UseVisualStyleBackColor = true;
            this.button_dw.Click += new System.EventHandler(this.button_dw_Click);
            // 
            // button_lft
            // 
            this.button_lft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_lft.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_lft.Location = new System.Drawing.Point(1, 44);
            this.button_lft.Margin = new System.Windows.Forms.Padding(1);
            this.button_lft.Name = "button_lft";
            this.button_lft.Size = new System.Drawing.Size(40, 41);
            this.button_lft.TabIndex = 8;
            this.button_lft.Text = "<";
            this.button_lft.UseVisualStyleBackColor = true;
            this.button_lft.Click += new System.EventHandler(this.button_lft_Click);
            // 
            // button_right
            // 
            this.button_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_right.Location = new System.Drawing.Point(85, 44);
            this.button_right.Margin = new System.Windows.Forms.Padding(1);
            this.button_right.Name = "button_right";
            this.button_right.Size = new System.Drawing.Size(40, 41);
            this.button_right.TabIndex = 9;
            this.button_right.Text = ">";
            this.button_right.UseVisualStyleBackColor = true;
            this.button_right.Click += new System.EventHandler(this.button_right_Click);
            // 
            // button_Ok
            // 
            this.button_Ok.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Ok.Location = new System.Drawing.Point(43, 44);
            this.button_Ok.Margin = new System.Windows.Forms.Padding(1);
            this.button_Ok.Name = "button_Ok";
            this.button_Ok.Size = new System.Drawing.Size(40, 41);
            this.button_Ok.TabIndex = 10;
            this.button_Ok.Text = "OK";
            this.button_Ok.UseVisualStyleBackColor = true;
            this.button_Ok.Click += new System.EventHandler(this.button_Ok_Click);
            // 
            // button_esc
            // 
            this.button_esc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_esc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_esc.Location = new System.Drawing.Point(85, 87);
            this.button_esc.Margin = new System.Windows.Forms.Padding(1);
            this.button_esc.Name = "button_esc";
            this.button_esc.Size = new System.Drawing.Size(40, 42);
            this.button_esc.TabIndex = 11;
            this.button_esc.Text = "Esc";
            this.button_esc.UseVisualStyleBackColor = true;
            this.button_esc.Click += new System.EventHandler(this.button_esc_Click);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.vScrollBar1.LargeChange = 3;
            this.vScrollBar1.Location = new System.Drawing.Point(551, 8);
            this.vScrollBar1.Maximum = 76;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(81, 418);
            this.vScrollBar1.TabIndex = 12;
            this.vScrollBar1.Value = 64;
            this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
            // 
            // label_fl
            // 
            this.label_fl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_fl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fl.ForeColor = System.Drawing.Color.DarkRed;
            this.label_fl.Location = new System.Drawing.Point(551, 448);
            this.label_fl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_fl.Name = "label_fl";
            this.label_fl.Size = new System.Drawing.Size(81, 40);
            this.label_fl.TabIndex = 13;
            this.label_fl.Text = "0";
            this.label_fl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_kapat
            // 
            this.button_kapat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_kapat.Location = new System.Drawing.Point(12, 449);
            this.button_kapat.Name = "button_kapat";
            this.button_kapat.Size = new System.Drawing.Size(125, 39);
            this.button_kapat.TabIndex = 14;
            this.button_kapat.Text = "Close";
            this.button_kapat.UseVisualStyleBackColor = true;
            this.button_kapat.Click += new System.EventHandler(this.button_kapat_Click);
            // 
            // button_arrow_up
            // 
            this.button_arrow_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_arrow_up.Location = new System.Drawing.Point(163, 448);
            this.button_arrow_up.Name = "button_arrow_up";
            this.button_arrow_up.Size = new System.Drawing.Size(42, 40);
            this.button_arrow_up.TabIndex = 15;
            this.button_arrow_up.Text = "Up";
            this.button_arrow_up.UseVisualStyleBackColor = true;
            this.button_arrow_up.Click += new System.EventHandler(this.button_arrow_up_Click);
            // 
            // button_arrow_down
            // 
            this.button_arrow_down.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_arrow_down.Location = new System.Drawing.Point(211, 448);
            this.button_arrow_down.Name = "button_arrow_down";
            this.button_arrow_down.Size = new System.Drawing.Size(42, 40);
            this.button_arrow_down.TabIndex = 16;
            this.button_arrow_down.Text = "Dw";
            this.button_arrow_down.UseVisualStyleBackColor = true;
            this.button_arrow_down.Click += new System.EventHandler(this.button_arrow_down_Click);
            // 
            // button_OverLoad
            // 
            this.button_OverLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_OverLoad.Location = new System.Drawing.Point(259, 448);
            this.button_OverLoad.Name = "button_OverLoad";
            this.button_OverLoad.Size = new System.Drawing.Size(42, 40);
            this.button_OverLoad.TabIndex = 17;
            this.button_OverLoad.Text = "OL";
            this.button_OverLoad.UseVisualStyleBackColor = true;
            this.button_OverLoad.Click += new System.EventHandler(this.button_OverLoad_Click);
            // 
            // button_outofservice
            // 
            this.button_outofservice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_outofservice.Location = new System.Drawing.Point(307, 448);
            this.button_outofservice.Name = "button_outofservice";
            this.button_outofservice.Size = new System.Drawing.Size(42, 40);
            this.button_outofservice.TabIndex = 18;
            this.button_outofservice.Text = "AoS";
            this.button_outofservice.UseVisualStyleBackColor = true;
            this.button_outofservice.Click += new System.EventHandler(this.button_outofservice_Click);
            // 
            // button_gong
            // 
            this.button_gong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_gong.Location = new System.Drawing.Point(464, 449);
            this.button_gong.Name = "button_gong";
            this.button_gong.Size = new System.Drawing.Size(42, 40);
            this.button_gong.TabIndex = 19;
            this.button_gong.Text = "Gng";
            this.button_gong.UseVisualStyleBackColor = true;
            this.button_gong.Click += new System.EventHandler(this.button_gong_Click);
            // 
            // button_door_close
            // 
            this.button_door_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_door_close.Location = new System.Drawing.Point(355, 449);
            this.button_door_close.Name = "button_door_close";
            this.button_door_close.Size = new System.Drawing.Size(42, 40);
            this.button_door_close.TabIndex = 20;
            this.button_door_close.Text = "><";
            this.button_door_close.UseVisualStyleBackColor = true;
            this.button_door_close.Click += new System.EventHandler(this.button_door_close_Click);
            // 
            // button_door_open
            // 
            this.button_door_open.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_door_open.Location = new System.Drawing.Point(403, 449);
            this.button_door_open.Name = "button_door_open";
            this.button_door_open.Size = new System.Drawing.Size(42, 40);
            this.button_door_open.TabIndex = 21;
            this.button_door_open.Text = "<>";
            this.button_door_open.UseVisualStyleBackColor = true;
            this.button_door_open.Click += new System.EventHandler(this.button_door_open_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.button_Ok, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button_up, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_right, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button_esc, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.button_dw, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button_lft, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 160);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(126, 130);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // F_test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 512);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.button_door_open);
            this.Controls.Add(this.button_door_close);
            this.Controls.Add(this.button_gong);
            this.Controls.Add(this.button_outofservice);
            this.Controls.Add(this.button_OverLoad);
            this.Controls.Add(this.button_arrow_down);
            this.Controls.Add(this.button_arrow_up);
            this.Controls.Add(this.button_kapat);
            this.Controls.Add(this.label_fl);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.cb_Baud);
            this.Controls.Add(this.rtb_Send);
            this.Controls.Add(this.cb_COMs);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSerialCom);
            this.Controls.Add(this.rtb_Receive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "F_test";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_Receive;
        private System.Windows.Forms.Button btnSerialCom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cb_COMs;
        private System.Windows.Forms.RichTextBox rtb_Send;
        private System.Windows.Forms.ComboBox cb_Baud;
        private System.Windows.Forms.Button button_up;
        private System.Windows.Forms.Button button_dw;
        private System.Windows.Forms.Button button_lft;
        private System.Windows.Forms.Button button_right;
        private System.Windows.Forms.Button button_Ok;
        private System.Windows.Forms.Button button_esc;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.Label label_fl;
        private System.Windows.Forms.Button button_kapat;
        private System.Windows.Forms.Button button_arrow_up;
        private System.Windows.Forms.Button button_arrow_down;
        private System.Windows.Forms.Button button_OverLoad;
        private System.Windows.Forms.Button button_outofservice;
        private System.Windows.Forms.Button button_gong;
        private System.Windows.Forms.Button button_door_close;
        private System.Windows.Forms.Button button_door_open;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

