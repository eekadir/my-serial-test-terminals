﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{

    public partial class F_test : Form
    {
        string CurrentPort = "";
        int CurrentBaud = 0;
        Thread COMReadTh;
        SerialPort COMPort = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
        bool RunLolaRun = true;
        bool UpdateUserSettings = false;

        bool esign_arrow_up = false;
        bool esign_arrow_down = false;
        bool esign_overload = false;
        bool esign_sevice = false;
        bool esign_gong = false;
        bool esign_door_open = false;
        bool esign_door_close = false;


        public F_test()
        {
            InitializeComponent();
            CurrentPort = Properties.Settings.Default.MyCOM;
            CurrentBaud = Properties.Settings.Default.MyBaud;
            cb_COMs.Text = CurrentPort;
            cb_Baud.Text = CurrentBaud.ToString();
            COMReadTh = new Thread(SerialRead);
            COMReadTh.Start();
        }

        private void SerialRead()
        {
            while (RunLolaRun)
            {
                if (COMPort.IsOpen)
                {
                    if (COMPort.BytesToRead > 0)
                    {
                        string str = COMPort.ReadExisting();

                        PutText(str);
                    }
                }
            }
        }

        public void PutText(string text)
        {
            if (Visible)
            {
                MethodInvoker invoker = new MethodInvoker(delegate
                {
                    rtb_Receive.AppendText(text);
                });

                this.Invoke(invoker);
            }
        }

        private void SaveText2File(RichTextBox rtb)
        {
            MethodInvoker invoker = new MethodInvoker(delegate
            {

                string filename = "Text";
                int index = 0;
                while (File.Exists(filename + index.ToString() + ".txt")) { index++; }

                rtb.SaveFile(filename + index.ToString() + ".txt");

                rtb.Clear();
            });

            this.Invoke(invoker);
        }

        bool btnSerialCom_ClickBlocked = false;
        private void btnSerialCom_Click(object sender, EventArgs e)
        {
            if (btnSerialCom_ClickBlocked == false)
            {
                btnSerialCom_ClickBlocked = true;


                if (COMPort.IsOpen)
                {
                    try
                    {
                        COMPort.Close();
                        btnSerialCom.Text = "Connect";
                    }
                    catch { }
                }
                else
                {
                    COMPort.PortName = cb_COMs.Text;
                    COMPort.BaudRate = Convert.ToInt32(cb_Baud.Text);

                    try
                    {
                        COMPort.Open();
                        btnSerialCom.Text = "Disconnect";
                        CurrentPort = COMPort.PortName;
                        CurrentBaud = COMPort.BaudRate;
                        UpdateUserSettings = true;
                    }
                    catch
                    {
                    }
                }

                btnSerialCom_ClickBlocked = false;
            }
        }

        private void cb_COMs_MouseDown(object sender, MouseEventArgs e)
        {
            cb_COMs.Items.Clear();
            cb_COMs.Items.AddRange(SerialPort.GetPortNames());
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (COMPort.IsOpen)
            {
                COMPort.Close(); ;
            }
            RunLolaRun = false;
            if (UpdateUserSettings)
            {
                Properties.Settings.Default.MyCOM = CurrentPort;
                Properties.Settings.Default.MyBaud = CurrentBaud;
                Properties.Settings.Default.Save();
            }
        }

        private void send_to_comport(string v)
        {
            if (COMPort.IsOpen)
            {
                COMPort.Write(v + "\r\n");
                rtb_Send.AppendText(v + "\r\n");
            }
        }

        
        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            label_fl.Text = (64 - vScrollBar1.Value).ToString();
            send_to_comport("FLNO" + label_fl.Text);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveText2File(rtb_Receive);
        }

        private void button_kapat_Click(object sender, EventArgs e)
        {
            COMReadTh.Abort();

            if (COMPort.IsOpen)
            {
                COMPort.Close();
            }

            this.Close();
        }

        private void button_up_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYU");
        }

        private void button_right_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYR");
        }

        private void button_dw_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYD");
        }

        private void button_lft_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYL");
        }

        private void button_Ok_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYO");
        }

        private void button_esc_Click(object sender, EventArgs e)
        {
            send_to_comport("KEYE");
        }

        private void button_arrow_up_Click(object sender, EventArgs e)
        {
            if (esign_arrow_up)
            {
                esign_arrow_up = false;
                button_arrow_up.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_arrow_up = true;
                button_arrow_up.BackColor = Color.LightGreen;
                send_to_comport("SIGN1");
            }
        }

        private void button_arrow_down_Click(object sender, EventArgs e)
        {
            if (esign_arrow_down)
            {
                esign_arrow_down = false;
                button_arrow_down.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_arrow_down = true;
                button_arrow_down.BackColor = Color.OrangeRed;
                send_to_comport("SIGN2");
            }
        }

        private void button_OverLoad_Click(object sender, EventArgs e)
        {
            if (esign_overload)
            {
                esign_overload = false;
                button_OverLoad.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_overload = true;
                button_OverLoad.BackColor = Color.OrangeRed;
                send_to_comport("SIGN3");
            }
        }

        private void button_outofservice_Click(object sender, EventArgs e)
        {
            if(esign_sevice)
            {
                esign_sevice = false;
                button_outofservice.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_sevice = true;
                button_outofservice.BackColor =Color.Red;
                send_to_comport("SIGN4");
            }
        }

        private void button_gong_Click(object sender, EventArgs e)
        {
            send_to_comport("ANNO" + label_fl.Text);
        }

        private void button_door_close_Click(object sender, EventArgs e)
        {
            if (esign_door_close)
            {
                esign_door_close = false;
                button_door_close.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_door_close = true;
                button_door_close.BackColor = Color.RosyBrown;
                send_to_comport("SIGN5");
            }
        }

        private void button_door_open_Click(object sender, EventArgs e)
        {
            if (esign_door_open)
            {
                esign_door_open = false;
                button_door_open.BackColor = SystemColors.Control;
                send_to_comport("SIGN0");
            }
            else
            {
                esign_door_open = true;
                button_door_open.BackColor = Color.SandyBrown;
                send_to_comport("SIGN6");
            }
        }
    }
}
